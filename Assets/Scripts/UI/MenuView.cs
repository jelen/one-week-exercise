﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuView : MonoBehaviour, GenericView
{
    [SerializeField]
    private Button _StartGameBtn = null;

    [SerializeField]
    private Button _ExitBtn = null;

    [SerializeField]
    private TextMeshProUGUI _Score = null;

    private GameController _Controller;

    public void Initialize(GameController controller){
        _Controller = controller;
        _StartGameBtn.onClick.AddListener(() => _Controller.ShowView(GameViews.SHIP_SELECTION));
        _ExitBtn.onClick.AddListener(_Controller.ExitGame);
    }

    public void Open(){
        this.gameObject.SetActive(true);
        RefreshScore();
    }

    public void Close(){
        this.gameObject.SetActive(false);
    }

    private void RefreshScore(){
        _Score.text = "Best score: " + PlayerPrefs.GetString("Best_score", "0");
    }
    
}
