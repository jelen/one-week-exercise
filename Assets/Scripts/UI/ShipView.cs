﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipView : MonoBehaviour, GenericView
{
    [SerializeField]
    private RectTransform _ShipPanelsContent = null;

    [SerializeField]
    private Button _StartGame = null;

    [SerializeField]
    private Button _GoBack = null;

    [SerializeField]
    private ToggleGroup _ShipGroup = null;

    private GameController _Controller;
    private List<ShipDetail> _ShipPanles;

    public void Initialize(GameController controller, List<ShipData> ships, Action<ShipData> callback){
        _Controller = controller;
        _StartGame.onClick.AddListener(() => _Controller.PlayerClickedStart());
        _GoBack.onClick.AddListener(() => _Controller.ShowView(GameViews.MENU));
        SetUpShipPanels(ships, callback);

        _ShipPanles[0].ToggleOn();
    }


    public void Open(){
        this.gameObject.SetActive(true);
    }

    public void Close(){
        this.gameObject.SetActive(false);
    }

    private void SetUpShipPanels(List<ShipData> ships, Action<ShipData> callback){
        _ShipPanles = new List<ShipDetail>();

        foreach(ShipData data in ships){
            ShipDetail panel = GameObject.Instantiate(PrefabBinder.Instance.ShipDetail);
            panel.transform.SetParent(_ShipPanelsContent, false);
            panel.SetUp(data, _ShipGroup, callback);

            _ShipPanles.Add(panel);
        }
    }
}
