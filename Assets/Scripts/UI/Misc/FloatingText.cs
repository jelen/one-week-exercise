﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class FloatingText : MonoBehaviour
{
    private static List<FloatingText> _TextPool;

    static FloatingText(){
        _TextPool = new List<FloatingText>();
    }

    [SerializeField]
    private TextMeshPro _Text = null;

    public void SetUp(string text, Vector3 worldPosition){
        _Text.text = text;
        this.transform.position = worldPosition;
        Animate();
    }

    private void Animate(){
        float y = this.transform.localPosition.y;
        this.transform.DOLocalMoveY(y + 1, 1.3f).OnComplete(Collect);
        this.gameObject.SetActive(true);
    }

    private void Collect(){
        this.gameObject.SetActive(false);
        this.transform.SetParent(null, false);
    }

    public static void ShowText(string text, Transform parent, Vector3 target){
        FloatingText pooledText = GetTextFromPool();
        pooledText.transform.SetParent(parent, false);
        pooledText.SetUp(text, target);
    }

    private static FloatingText GetTextFromPool(){
        foreach(FloatingText text in _TextPool){
            if(text.gameObject.activeInHierarchy == false)
                return text;
        }
        
        return AddNewTextToPool();
    }

    private static FloatingText AddNewTextToPool(){
        FloatingText newText = GameObject.Instantiate(PrefabBinder.Instance.FloatingText);
        newText.gameObject.SetActive(false);
    
        _TextPool.Add(newText);

        return newText;
    }
}
