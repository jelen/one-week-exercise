﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class ShipDetail : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _Name = null;

    [SerializeField]
    private TextMeshProUGUI _AttackSpeed = null;

    [SerializeField]
    private TextMeshProUGUI _MovementSpeed = null;

    [SerializeField]
    private TextMeshProUGUI _Health = null;

    [SerializeField]
    private TextMeshProUGUI _ShieldCap = null;

    [SerializeField]
    private TextMeshProUGUI _ShieldRecharge = null;

    [SerializeField]
    private Image _ShipIcon = null;

    private Toggle _Toggle;
    private Action _Callback;

    public void SetUp(ShipData ship, ToggleGroup group, Action<ShipData> callback){
        _Name.text =  ship.ShipName;
        _AttackSpeed.text = "fire rate: " + ship.AttackSpeed;
        _MovementSpeed.text = "speed: " + ship.Speed;
        _Health.text = "HP: " + ship.Health;
        _ShieldCap.text = "Shield cap: " + ship.ShieldCapacity;
        _ShieldRecharge.text = "Shield regen: " + ship.ShieldRecharge;
        _ShipIcon.sprite = ship.ShipIcon;

        _Callback = () => callback.Invoke(ship);
        _Toggle = this.GetComponent<Toggle>();
        _Toggle.group = group;
        _Toggle.onValueChanged.RemoveAllListeners();
        _Toggle.onValueChanged.AddListener(Toggle);
    }

    public void ToggleOn(){
        _Toggle.onValueChanged.Invoke(true);
    }

    private void Toggle(bool on){
        if(on)
            _Callback?.Invoke();
    }
}
