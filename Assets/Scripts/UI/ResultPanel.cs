﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResultPanel : MonoBehaviour, GenericView
{   
    [SerializeField]
    private Button _Menu = null;

    [SerializeField]
    private TextMeshProUGUI _Score = null;

    [SerializeField]
    private TextMeshProUGUI _Result = null;

    private GameController _Controller;
    private BattleController _BattleController;

    public void Initialize(GameController controller, BattleController battleController){
        _Controller = controller;
        _BattleController = battleController;
        _Menu.onClick.RemoveAllListeners();
        _Menu.onClick.AddListener(() => _Controller.ShowView(GameViews.MENU));  
    }

    public void Open(){
        this.gameObject.SetActive(true);
        Refresh();
    }

    public void Close(){
        this.gameObject.SetActive(false);
    }

    private void Refresh(){
        _Score.text = "Score: " + _BattleController.PlayerScore.ToString();
        _Result.text = (_Controller.LastGameResult) ? "You won!" : "You lose";
    }
}
