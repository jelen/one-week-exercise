﻿using System.Globalization;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BattleView : MonoBehaviour, GenericView
{
    [SerializeField]
    private TextMeshProUGUI _Score = null;

    [SerializeField]
    private TextMeshProUGUI _Stage = null;

    [SerializeField]
    private TextMeshProUGUI _Wave = null;

    [SerializeField]
    private TextMeshProUGUI _Special = null;

    [SerializeField]
    private TextMeshProUGUI _Health = null;
    
    private BattleController _BattleController;

    public void Initialize(BattleController battleController){
        _BattleController = battleController;

    }

    void LateUpdate(){
        if(_BattleController == null && _BattleController.BattleOnging)
            return;

        _Score.text = "Score: " + _BattleController.PlayerScore.ToString();
        _Wave.text = "Wave: " + (_BattleController.CurrentWave + 1).ToString() + "/" + _BattleController.TotalWave.ToString();
        _Stage.text = "Stage: " + (_BattleController.CurrentStage + 1).ToString();
        _Health.text = "Health: " +  _BattleController.PlayerHealth.ToString();
    }

    public void Open(){
        this.gameObject.SetActive(true);
    }

    public void Close(){
        this.gameObject.SetActive(false);
    }
}
