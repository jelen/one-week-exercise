﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface GenericView
{
    void Open();
    void Close();
    
}
