﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Bezier
{
    public Vector2 StartingPoint;
    public Vector2 EndingPoint;
    public Vector2 FirstControlPoint;
    public Vector2 SecondControlPoint;
    private float _InternalTime;

    public Bezier(Vector2 startPoint, Vector2 firstControlPoint, Vector2 endPoint, Vector2 secondControlPoint){
        _InternalTime = 0;
        StartingPoint = startPoint;
        EndingPoint = endPoint;
        FirstControlPoint = firstControlPoint;
        SecondControlPoint = secondControlPoint;
    }

    public Vector2 NextStep(float timeStep, out bool reachedEnd){
        _InternalTime += timeStep;
        if(_InternalTime > 1)
            _InternalTime = 1;
        
        reachedEnd = _InternalTime == 1;

        return Bezier.BezierPoint(StartingPoint, EndingPoint, FirstControlPoint, SecondControlPoint, _InternalTime);
    }

    public void ResetTime(){
        _InternalTime = 0;
    }

    public Vector2 GetPoint(float time){
        return Bezier.BezierPoint(StartingPoint, EndingPoint, FirstControlPoint, SecondControlPoint, time);
    }

    public static Vector2 BezierPoint(Vector2 startPoint, Vector2 firstControlPoint, Vector2 endPoint, Vector2 secondControlPoint, float time){
        Vector2 interpolatedPoint = Vector2.zero;
        interpolatedPoint += startPoint * Mathf.Pow(1 - time, 3);
        interpolatedPoint += firstControlPoint * Mathf.Pow(time, 3);
        interpolatedPoint += endPoint * 3 * time * Mathf.Pow(1 - time, 2);
        interpolatedPoint += secondControlPoint * 3 * (1 - time) * Mathf.Pow(time, 2); 

        return interpolatedPoint;
    }
}
