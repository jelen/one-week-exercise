﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabBinder : MonoBehaviour
{
    public static PrefabBinder Instance;

    [SerializeField]
    private GameObject _MenuView = null;

    [SerializeField]
    private GameObject _ShipView = null;

    [SerializeField]
    private GameObject _GameplayView = null;

    [SerializeField]
    private GameObject _ResultScreen = null;

    [SerializeField]
    private ShipDetail _ShipDetail = null;

    [SerializeField]
    private Bullet _PlayerBullet = null;

    [SerializeField]
    private Bullet _EnemyBullet = null;

    [SerializeField]
    private Bullet _SpikeBullet = null;

    [SerializeField]
    private FloatingText _FloatingText = null;

    public GameObject MenuView{
        get{
            return _MenuView;
        }
    }

    public GameObject ShipView{
        get{
            return _ShipView;
        }
    }

    public GameObject GameplayView{
        get{
            return _GameplayView;
        }
    }

    public GameObject ResultView{
        get{
            return _ResultScreen;
        }
    }

    public ShipDetail ShipDetail{
        get{
            return _ShipDetail;
        }
    }

    public Bullet PlayerBullet{
        get{
            return _PlayerBullet;
        }
    }

    public Bullet EnemyBullet{
        get{
            return _EnemyBullet;
        }
    }

    public Bullet SpikeBullet{
        get{
            return _SpikeBullet;
        }
    }

    public FloatingText FloatingText{
        get{
            return _FloatingText;
        }
    }

    void Awake(){
        if(PrefabBinder.Instance == null)
            PrefabBinder.Instance = this;
        else
            DestroyImmediate(this.gameObject);
    }
}
