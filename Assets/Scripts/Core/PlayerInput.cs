﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public static event Action<float> Movement;
    public static event Action<bool> Shooting;
    public static event Action Special;
    public static event Action<bool> Shield;

    private bool _ShootMemory;

    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        bool shoot = Input.GetAxisRaw("Shoot") > 0;
        bool special = Input.GetAxisRaw("Special") > 0;
        bool shield = Input.GetAxisRaw("Shield") > 0;

        if(horizontal != 0)
            PlayerInput.Movement?.Invoke(horizontal);
        
        if(shoot != _ShootMemory)
            PlayerInput.Shooting?.Invoke(shoot);

        if(special)
            PlayerInput.Special?.Invoke();
        
        PlayerInput.Shield?.Invoke(shield);

        _ShootMemory = shoot;

    }

}
