﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage{
    public int Index;
    public Formation Formation;
    public EnemyData[] AllowedEnemies;
    public List<Wave> AllWaves;

    public Stage(int index, int wavesCount, List<EnemyData> enemyTypes){
        Index = index;
        AllWaves = new List<Wave>(wavesCount);
        AllowedEnemies = new EnemyData[4];
        Formation = FormationDefinitions.GetRandomFormation();

        foreach(EnemyData data in enemyTypes)
            AllowedEnemies[data.Tier] = data;

        for(int i = 0; i < wavesCount; i++){
            Wave wave = new Wave(index*(4 + 2*wavesCount) + 10);
            PopulateWave(wave);
            AllWaves.Add(wave);
        }

    }

    public void FreeFormationSlot(int tier, int index){
        index = tier * 100 + index;
        Formation.TakenSlots.Remove(index);
    }

    public Vector3 GetFreeFormationSlot(int tier, out int index){
        List<int> indices = new List<int>();
        for(int i = 0; i < Formation.Slots[tier].Count; i++)
            if(Formation.TakenSlots.Contains(tier * 100 + i) == false)
                indices.Add(i);

        int chosneIndex = indices[UnityEngine.Random.Range(0, indices.Count)];
        Formation.TakenSlots.Add(tier * 100 + chosneIndex);
        
        index = chosneIndex;
        return Formation.Slots[tier][chosneIndex];
    } 

    private void PopulateWave(Wave wave){
        int population = wave.TotalEnemies;
        List<int> indices = new List<int>();

        for(int i = 0; i < AllowedEnemies.Length; i++){
            if(AllowedEnemies[i] != null){
                indices.Add(i);
                if(i == 1 || i ==2)
                    indices.Add(i);
            }
        }

        while(population > 0){
            int tierIndex = UnityEngine.Random.Range(0, indices.Count);

            wave.EnemiesInTier[AllowedEnemies[indices[tierIndex]].Tier] += 2;
            population -= 2;
        }
    }

    public class Wave{
        public int TotalEnemies;
        public int[] EnemiesInTier;

        public Wave(int totalEnemies){
            TotalEnemies = totalEnemies;
            EnemiesInTier = new int[4];
        }
    }
}
