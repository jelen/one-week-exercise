﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ShipData : ScriptableObject
{
    public string ShipName;
    public float Speed;
    public int Health;
    public float AttackSpeed;
    public float SpecialCharge;
    public float ShieldCapacity;
    public float ShieldRecharge;
    public Sprite ShipIcon;
    public GameObject ShipPrefab;

}
