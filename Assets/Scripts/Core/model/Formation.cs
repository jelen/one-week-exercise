﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Formation
{
    public int TotalSlots;
    public List<Vector3>[] Slots;
    public HashSet<int> TakenSlots;
    
}
