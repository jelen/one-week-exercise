﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemyData : ScriptableObject
{
    public string Name;
    public int ScoreValue;
    public int Health;
    [Range(0, 3)]
    public int Tier;
    public float Speed;
    public bool CanDesertPack;
    public Enemy Prefab;

}
