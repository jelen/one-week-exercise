﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarmationHandler : MonoBehaviour
{
    public Formation CurrentFormation;
    
    public void NewFormation(){
        CurrentFormation = FormationDefinitions.GetRandomFormation();
    }
    
}


public static class FormationDefinitions{

    public static Formation GetRandomFormation(){
        if(UnityEngine.Random.Range((int)0, 2) == 0)
            return FormationOne();
        else
            return FormationTwo();
    }

    private static Formation FormationOne(){
        Formation formation = new Formation();
        Prepare(ref formation);

        //Tier 3 loop
        for(int i = 0; i < 3; i++){
            formation.Slots[3].Add(new Vector3(0.25f + i * 0.065f, 0.8f));
            formation.Slots[3].Add(new Vector3(0.65f + i * 0.065f, 0.8f));
            formation.TotalSlots += 2;
        }

        //Tier 2 loop
        for(int i = 0; i < 8; i++){
            formation.Slots[2].Add(new Vector3(0.225f + (i%4) * 0.065f, 0.7f - i/4 * 0.065f));
            formation.Slots[2].Add(new Vector3(0.625f + (i%4) * 0.065f, 0.7f - i/4 * 0.065f));
            formation.TotalSlots += 2;
        }

        //Tier 1 loop
        for(int i = 0; i < 20; i++){
            formation.Slots[1].Add(new Vector3(0.225f + (i%10) * 0.065f, 0.6f - i/10 * 0.065f));
            formation.TotalSlots += 1;
        }

        //Tier 0 loop
        for(int i = 0; i < 20; i++){
            formation.Slots[0].Add(new Vector3(0.225f + (i%10) * 0.065f, 0.5f));
            formation.TotalSlots += 1;
        }

        return formation;
    }

    private static Formation FormationTwo(){
        Formation formation = new Formation();
        Prepare(ref formation);

        //Tier 3 loop
        for(int i = 0; i < 8; i++){
            formation.Slots[3].Add(new Vector3(0.375f + (i%4) * 0.065f, 0.75f - (i/4) * 0.065f));
            formation.TotalSlots += 1;
        }

        //Tier 2 loop
        for(int i = 0; i < 12; i++){
            formation.Slots[2].Add(new Vector3(0.35f + (i%6) * 0.065f, 0.65f - (i/6) * 0.065f));
            formation.TotalSlots += 1;
        }

        //Tier 1 loop
        for(int i = 0; i < 16; i++){
            formation.Slots[1].Add(new Vector3(0.325f + (i%8) * 0.065f, 0.55f - (i/8) * 0.065f));
            formation.TotalSlots += 1;
        }

        //Tier 0 loop
        for(int i = 0; i < 10; i++){
            formation.Slots[0].Add(new Vector3(0.3f + (i%10) * 0.065f, 0.45f));
            formation.TotalSlots += 1;

        }

        return formation;
    }

    private static void Prepare(ref Formation formation){
        int slotAmount = 4;
        formation.Slots = new List<Vector3>[slotAmount];
        for(int i = 0; i < slotAmount; i++)
            formation.Slots[i] = new List<Vector3>();

        formation.TakenSlots = new HashSet<int>();
        formation.TotalSlots = 0;
    }
}