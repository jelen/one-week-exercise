﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{   
    [SerializeField]
    private List<ShipData> _ShipTypes = null;

    [SerializeField]
    private PrefabBinder _PrefabBinder = null;

    [SerializeField]
    private BattleController _BattleController = null;

    [SerializeField]
    private RectTransform _UIRoot = null;

    private GameViews _CurrentView;
    private GenericView[] _GameViews;
    private ShipData _ChosenShipType;

    public ShipData ChosenShip{
        get{
            return _ChosenShipType;
        }
    }

    public bool LastGameResult;

    #region MonoBehaviour callbacks
    void Start()
    {
        _GameViews = new GenericView[System.Enum.GetValues(typeof(GameViews)).Length];
        _BattleController.SessionEnded += EndGame;

        LoadUIPrefabs();
        ShowView(GameViews.MENU);
    }



    #endregion

    #region Public methods
    public void PlayerClickedStart(){
        _BattleController.InitializeBattle(this);
        _BattleController.StartBattle();

        ShowView(GameViews.GAMEPLAY);
    }

    public void ShowView(GameViews viewType){
        _GameViews[(int)_CurrentView].Close();
        _GameViews[(int)viewType].Open();
        _CurrentView = viewType;
    }

    public void ExitGame(){
        Application.Quit();
        // Debug.Log("Quit");
    }

    #endregion

    #region private methods
    private void LoadUIPrefabs(){
        MenuView menu = GameObject.Instantiate(_PrefabBinder.MenuView).GetComponent<MenuView>();
        menu.transform.SetParent(_UIRoot, false);
        menu.Initialize(this);
        menu.Close();

        ShipView shipView = GameObject.Instantiate(_PrefabBinder.ShipView).GetComponent<ShipView>();
        shipView.transform.SetParent(_UIRoot, false);
        shipView.Initialize(this, _ShipTypes, ship => _ChosenShipType = ship);
        shipView.Close();

        BattleView gameplayView = GameObject.Instantiate(_PrefabBinder.GameplayView).GetComponent<BattleView>();
        gameplayView.transform.SetParent(_UIRoot, false);
        gameplayView.Initialize(_BattleController);
        gameplayView.Close();

        ResultPanel result = GameObject.Instantiate(_PrefabBinder.ResultView).GetComponent<ResultPanel>();
        result.transform.SetParent(_UIRoot, false);
        result.Initialize(this, _BattleController);
        result.Close();


        _GameViews[(int)GameViews.MENU] = menu;
        _GameViews[(int)GameViews.SHIP_SELECTION] = shipView;
        _GameViews[(int)GameViews.GAMEPLAY] = gameplayView;
        _GameViews[(int)GameViews.RESULT] = result;
    }

    private void EndGame(bool playerWon){
        long savedScore = long.Parse(PlayerPrefs.GetString("Best_score", "0"));
        if(savedScore < _BattleController.PlayerScore)
            PlayerPrefs.SetString("Best_score", _BattleController.PlayerScore.ToString());

        LastGameResult = playerWon;
        _BattleController.CleanUp();
        ShowView(GameViews.RESULT);

    }

    #endregion

}

public enum GameViews{
    MENU,
    SHIP_SELECTION,
    GAMEPLAY,
    RESULT
}