﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleController : MonoBehaviour
{
    public event Action<bool> SessionEnded;

    public List<EnemyData> AllowedEnemies;

    private GameController _Controller;
    private Transform _Field;
    private List<Stage> _SessionStages;
    private List<Enemy> _CurrentWaveSpawns;
    private Camera _MainCamera;
    
    private long _Score;
    private int _PlayerHealth;
    private int _SpecialCharge;
    private int _CurrentStage;
    private int _CurrentWave;
    private int _EnemiesLeft;
    private int[] _EnemiesSpawned;//Contains 5 int's, 0 to 3 corresponds to enemy tier, 4 is total amount of enemies
    private int _EnemiesFlying;
    private int _AssultingUnits;
    private bool _BattleOnging;
    private bool _SpawningReady;
    private PlayerShip _CurrentShip;

    public long PlayerScore{
        get{
            return _Score;
        }
    }

    public int PlayerHealth{
        get{
            return _PlayerHealth;
        }
    }

    public bool IsSpecialReady{
        get{
            return _SpecialCharge >= _Controller.ChosenShip.SpecialCharge;
        }
    }

    public float SpecialChargePercent{
        get{
            return _SpecialCharge / _Controller.ChosenShip.SpecialCharge;
        }
    }

    public int CurrentWave{
        get{
            return _CurrentWave;
        }
    }

    public int TotalWave{
        get{
            return _SessionStages[_CurrentStage].AllWaves.Count;
        }
    }

    public int CurrentStage{
        get{
            return _CurrentStage;
        }
    }

    public int AssultLimit{
        get{
            return _CurrentStage + _CurrentWave + 1;
        }
    }

    public bool AssultLimitReached{
        get{
            return _AssultingUnits >= AssultLimit;
        }
    }

    public bool BattleOnging{
        get{
            return _BattleOnging;
        }
    }

    public Transform PlayerPos{
        get{
            return _CurrentShip.transform;
        }
    }

    void Update(){
        if(_BattleOnging == false)
            return;
        
        if(_SpawningReady && _EnemiesSpawned[4] <= _SessionStages[_CurrentStage].AllWaves[_CurrentWave].TotalEnemies){
            StartCoroutine(SpawnEnemies());
        }

        if(AssultLimitReached == false && UnityEngine.Random.Range(0, 100) > 35 && _CurrentWaveSpawns.Count > 0){
            Enemy chosenOne = _CurrentWaveSpawns[UnityEngine.Random.Range(0, _CurrentWaveSpawns.Count)];
            AssultStarted(chosenOne);
        }
    }

    public void InitializeBattle(GameController controller){
        if(_MainCamera == null)
            _MainCamera = Camera.main;

        _SessionStages = new List<Stage>();
        _EnemiesSpawned = new int[5];
        _Controller = controller;
        _PlayerHealth = controller.ChosenShip.Health;
        _SpecialCharge = 0;
        _Score = 0;
        _CurrentStage = -1;
        _Field = new GameObject().GetComponent<Transform>();
        _Field.name = "Field";
        _Field.SetParent(this.transform, false);
        _Field.localPosition = Vector3.zero;

        PrepareStages(2);

        PlayerShip player = GameObject.Instantiate(controller.ChosenShip.ShipPrefab).GetComponent<PlayerShip>();
        player.transform.SetParent(_Field, false);
        player.transform.position = _MainCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.05f, 10));
        player.Initialize(controller, this);

        _CurrentShip = player;
    }

    public void StartBattle(){
        NextStage();
        _SpawningReady = true;
    }

    public void PlayerUsedSpecial(){
        _SpecialCharge = 0;
    }

    public void PlayerHit(){
        _PlayerHealth -= 1;
        
        if(_PlayerHealth <= 0)
            SessionEnded.Invoke(false);
    }

    public void CleanUp(){
        _BattleOnging = false;
        Destroy(_Field.gameObject);
    }

    public void EnemyKilled(Enemy enemy){
        _Score += enemy.ScoreValue;
        _SpecialCharge += 1;
        _EnemiesLeft -= 1;
        _SessionStages[_CurrentStage].FreeFormationSlot(enemy.Tier, enemy.FormationSlotIndex);
        _CurrentWaveSpawns.Remove(enemy);

        FloatingText.ShowText(enemy.ScoreValue.ToString(), enemy.transform.parent, enemy.transform.position);

        if(enemy.IsFlying)
           EnemyReturned(enemy);
        
        if(enemy.Attacking)
            _AssultingUnits -= 1;

        Destroy(enemy.gameObject);

        if(_EnemiesLeft == 0)
            StageFinished();
    }

    private void PrepareStages(int count){
        List<EnemyData> allowedEnemies = new List<EnemyData>();

        for(int i = 0; i < count; i++){
            Stage stage = new Stage(i, 1 + (i*2), AllowedEnemies);
            _SessionStages.Add(stage);
        }
    }

    private void StageFinished(){
        if(_CurrentWave < _SessionStages[_CurrentStage].AllWaves.Count - 1)
            NextWave();
        else if(_CurrentStage < _SessionStages.Count - 1)
            NextStage();
        else
            SessionEnded?.Invoke(true);
    }
 
    private void NextStage(){
        _CurrentStage += 1;
        _CurrentWave = -1;

        NextWave();

        _BattleOnging = true;
    }

    private void NextWave(){
        _CurrentWave += 1;
        Stage.Wave wave = _SessionStages[_CurrentStage].AllWaves[_CurrentWave];
        _EnemiesLeft = wave.TotalEnemies;
        _EnemiesSpawned = new int[5];
        _CurrentWaveSpawns = new List<Enemy>();
        _EnemiesFlying = 0;
        _SpawningReady = true;
    }

    private IEnumerator SpawnEnemies(){
        _SpawningReady = false;
        int enemiesSpawned = 0;
        Stage stage = _SessionStages[_CurrentStage];
        Stage.Wave wave = stage.AllWaves[_CurrentWave];
        Bezier path = RandomEnemyPath();
        int packSize = UnityEngine.Random.Range(4, 10);
        packSize = Mathf.Min(packSize, wave.TotalEnemies - _EnemiesSpawned[4]);
        
        while(packSize > 0){
            EnemyData enemyType = stage.AllowedEnemies[UnityEngine.Random.Range(0, stage.AllowedEnemies.Length)];
            if(_EnemiesSpawned[enemyType.Tier] >= wave.EnemiesInTier[enemyType.Tier])
                continue;

            if(stage.Formation.TotalSlots == stage.Formation.TakenSlots.Count)
                break;

            Enemy enemy = SpawnEnemy(enemyType);
            enemy.FlyIn(path, true);
            _CurrentWaveSpawns.Add(enemy);
            packSize -= 1;
            enemiesSpawned += 1;
            yield return new WaitForSeconds(0.3f);

        }

        _EnemiesSpawned[4] += enemiesSpawned;
    }

    private void SentToFormation(Enemy enemy){
        Vector3 endPoint;
        Stage stage = _SessionStages[_CurrentStage];
        if(enemy.FormationSlotIndex == -1)
            endPoint = stage.GetFreeFormationSlot(enemy.Tier, out enemy.FormationSlotIndex);
        else
            endPoint = stage.Formation.Slots[enemy.Tier][enemy.FormationSlotIndex];

        endPoint = _MainCamera.ViewportToWorldPoint(endPoint);

        Bezier path = new Bezier(enemy.transform.position, enemy.BezierPath.SecondControlPoint, endPoint, enemy.BezierPath.FirstControlPoint);
        enemy.FinishedEntrance -= SentToFormation;
        enemy.FormationPos = endPoint;
        enemy.FlyIn(path);
    }

    private void AssultStarted(Enemy enemy){
        if(enemy.Attacking || enemy.IsFlying)
            return;

        _AssultingUnits += 1;
        enemy.AssultPlayer(PlayerPos);
    }

    private void AssultEnded(Enemy enemy){
        _AssultingUnits -= 1;
        if(_AssultingUnits < 0)
            _AssultingUnits = 0;

        SentToFormation(enemy);
    }

    private void EnemyTookOf(Enemy enemy){
        _EnemiesFlying += 1;
        _SpawningReady = false;
    }

    private void EnemyReturned(Enemy enemy){
        _EnemiesFlying -= 1;
        if(_EnemiesFlying == 0)
            _SpawningReady = true;
    }

    private Bezier RandomEnemyPath(){
        float spawnSide = UnityEngine.Random.Range(0, 3);//! 0 -> left, 1 -> up, 2 -> right
        Vector3 startPoint, endPoint, controllPoint1, controllPoint2;
        float spawnX, spawnY;

        if(spawnSide == 1){
            spawnY = 1.1f;
            spawnX = UnityEngine.Random.Range(0.3f, 1);
        }else{
            spawnX = (spawnSide == 0) ? -0.1f : 1.1f;
            spawnY = UnityEngine.Random.Range(0.3f, 1);
        }
    
        startPoint = new Vector3(spawnX, spawnY, 10);
        endPoint = new Vector3(UnityEngine.Random.Range(0.3f, 1), UnityEngine.Random.Range(0.3f, 1), 10);
        controllPoint1 = new Vector3(UnityEngine.Random.Range(0.3f, 1), UnityEngine.Random.Range(0.3f, 1), 10);
        controllPoint2 = new Vector3(UnityEngine.Random.Range(0.3f, 1), UnityEngine.Random.Range(0.3f, 1), 10);

        startPoint = _MainCamera.ViewportToWorldPoint(startPoint);
        endPoint = _MainCamera.ViewportToWorldPoint(endPoint);
        controllPoint1 = _MainCamera.ViewportToWorldPoint(controllPoint1);
        controllPoint2 = _MainCamera.ViewportToWorldPoint(controllPoint2);

        return new Bezier(startPoint, controllPoint1, endPoint, controllPoint2);
    }

    private Enemy SpawnEnemy(EnemyData enemyType){
        Enemy enemy = GameObject.Instantiate(enemyType.Prefab);
        enemy.transform.SetParent(_Field, false);
        enemy.transform.position = new Vector2(-100, -100);
        enemy.Initialize(this, enemyType);
        enemy.FinishedEntrance += SentToFormation;
        enemy.TakeOff += EnemyTookOf;
        enemy.ReturnedToFormation += EnemyReturned;
        enemy.AssultFinished += AssultEnded;

        return enemy;
    }

}
