﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{   
    [SerializeField]
    private Rigidbody2D _RBody = null;


    public bool PlayerShot = false;
    public float Speed;
    public int Pierce;
    public bool Ready;

    private Camera _MainCamera;
    private Vector2 _Direction;

    void FixedUpdate()
    {
        if(Ready == false)
            return;

        Vector2 pos = _RBody.position;
        pos += _Direction * (Speed * Time.fixedDeltaTime);

        _RBody.position = pos;
    }

    void LateUpdate(){
        if(Ready == false)
            return;

        Vector2 viewport = _MainCamera.WorldToViewportPoint(this.transform.position);
        if(viewport.y > 1.2 || viewport.y < -0.2)
            Collect();

    }
    
    public void SetUp(Vector3 spawnPos, Vector2 direction, bool playerShot, float speed = 15, int pierce = 0){
        _MainCamera = Camera.main;
        _Direction = direction;

        Speed = speed;
        Pierce = pierce;
        PlayerShot = playerShot;
        this.transform.localPosition = spawnPos;

        Ready = true;
    }

    public void EnemyHitted(){
        Pierce -= 1;

        if(Pierce <= 0)
            Collect();
    }

    public void Collect(){
        Ready = false;

        Destroy(this.gameObject);
    }

}
