﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShip : Entity
{
    [SerializeField]
    private Transform[] _BulletSpawn = null;

    [SerializeField]
    private Image _ShieldFill = null;
    
    [SerializeField]
    private Image _SpecialFill = null;

    [SerializeField]
    private GameObject _ShieldImage = null;

    private float _AttackSpeed;
    private float _ShieldRecharge;
    private float _AttackSpeedMultiplier = 1;
    private float _AttackCooldown;
    private float _SpecialTimer = 0;
    private float _ShieldTimer = 0;
    private float _MaxSpecial;
    private float _MaxShield;
    private bool _SpecialActive;
    private bool _ShieldActive;
    private bool _ShieldCooling;
    private GameController _Controller;
    private BattleController _BattleController;
    private Camera _MainCamera;
    
    void Awake(){
        _Player = true;
        _MainCamera = Camera.main;

        PlayerInput.Movement += Move;
        PlayerInput.Shooting += OnShootPressed;
        PlayerInput.Special += ActivateSpecial;
        PlayerInput.Shield += ShieldSwitch;
    }

    void OnDestroy(){
        Collect();
    }

    void Update(){
        // if(_AttackCooldown > 0){
        //     _AttackCooldown -= Time.deltaTime;
        //     if(_AttackCooldown < 0)
        //         _AttackCooldown = 0;
        // }

        if(_SpecialActive){
            _SpecialTimer -= Time.deltaTime;
            if(_SpecialTimer <= 0){
                EndSpecial();
            }
            
            OnShootPressed(true);
        }

        if(_ShieldActive){
            _ShieldTimer -= Time.deltaTime;
            if(_ShieldTimer <= 0){
                ShieldSwitch(false);
                _ShieldTimer = 0;
                _ShieldCooling = true;
            }
        }else{
            _ShieldTimer += _ShieldRecharge * Time.deltaTime;
            if(_ShieldTimer > _MaxShield / 3)
                _ShieldCooling = false;

            if(_ShieldTimer > _MaxShield)
                _ShieldTimer = _MaxShield;
        }

        _ShieldImage.SetActive(_ShieldActive);
        RefreshUI();
    }

    public void Initialize(GameController controller, BattleController battleController){
        _Controller = controller;
        _BattleController = battleController;
        _AttackSpeed = controller.ChosenShip.AttackSpeed;
        _MovementSpeed = controller.ChosenShip.Speed;
        _MaxSpecial = controller.ChosenShip.SpecialCharge;
        _MaxShield = controller.ChosenShip.ShieldCapacity;
        _ShieldRecharge = controller.ChosenShip.ShieldRecharge;
        _ShieldTimer = _MaxShield;

        Health = controller.ChosenShip.Health;
    }

    public void Move(float direction){
        Vector2 pos = transform.position;
        pos.x += direction * _MovementSpeed * Time.deltaTime;
        ClampPlayerPosition(ref pos);
        transform.position = pos;
    }

    private void ShieldSwitch(bool active){
        if(_ShieldCooling == false)
            _ShieldActive = active;
    }

    public virtual void OnShootPressed(bool pressed){
        if(pressed == false)
            return;

        foreach(Transform spawn in _BulletSpawn){
            Bullet newBullet = GameObject.Instantiate(PrefabBinder.Instance.PlayerBullet);
            newBullet.transform.SetParent(this.transform.parent, false);
            newBullet.SetUp(spawn.position, Vector2.up, true, 20);
        }

        _AttackCooldown = 60 / (_AttackSpeed * _AttackSpeedMultiplier);
    }

    public virtual void ActivateSpecial(){
        if(_BattleController.IsSpecialReady == false)
            return;
        
        _AttackSpeedMultiplier = 3;
        _SpecialTimer = _MaxSpecial;
        _SpecialActive = true;
        _BattleController.PlayerUsedSpecial();
    }

    protected void EndSpecial(){
        _SpecialActive = false;
        _SpecialTimer = 0;
        _AttackSpeedMultiplier = 1;
    }

    protected void RefreshUI(){
        if(_SpecialActive)
            _SpecialFill.fillAmount = _SpecialTimer / _MaxSpecial;
        else
            _SpecialFill.fillAmount = _BattleController.SpecialChargePercent;

        _ShieldFill.fillAmount = _ShieldTimer / _MaxShield;
    }

    protected override void TakeHit(){
        if(_ShieldActive)
            return;

        _BattleController.PlayerHit();

    }

    protected override void Collect(){
        base.Collect();
        PlayerInput.Movement -= Move;
        PlayerInput.Shooting -= OnShootPressed;
        PlayerInput.Special -= ActivateSpecial;
    }

    private void ClampPlayerPosition(ref Vector2 position){
        Vector2 viewportPos = _MainCamera.WorldToViewportPoint(position);
        viewportPos.x = Mathf.Clamp(viewportPos.x, 0.03f, 0.97f);
        position = _MainCamera.ViewportToWorldPoint(viewportPos);
    }

}
