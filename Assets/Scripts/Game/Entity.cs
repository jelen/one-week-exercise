﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    protected bool _Player = false;

    protected float _MovementSpeed = 40;

    public virtual int Health{
        get;
        set;
    }

    protected virtual void TakeHit(){
        if(Health <= 0)
            return;

        Health -= 1;
        if(Health <= 0)
            Collect();
    }

    protected void OnTriggerEnter2D(Collider2D col){
        Bullet bullet = col.GetComponentInParent<Bullet>();
        if(bullet != null && bullet.PlayerShot != _Player && bullet.Ready){
            bullet.EnemyHitted();
            TakeHit();
        }else if(_Player && col.tag.Equals("Enemy"))
            TakeHit();
    }

    protected virtual void Collect(){
        Destroy(this.gameObject);
    }

}
