﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]

public class EntranceAction : MonoBehaviour, EnemyAction
{   
    private bool _Entering;
    private float _Speed;
    private Enemy _Parent;
    private Bezier _EntrancePath;

    public void SetUp(Enemy parent){
        _Parent = parent;
    }

    public void UpdateTick(float time){
        bool endPointReached;
        float step = 1/(60/_Speed);
        float interpolationStep = Time.deltaTime * step;
        Vector2 nextPos = _EntrancePath.NextStep(interpolationStep, out endPointReached);

        Vector2 deltaPos = (Vector2)this.transform.position - nextPos;
        deltaPos.Normalize();
        float z = Mathf.Atan2(deltaPos.x, deltaPos.y) * Mathf.Rad2Deg;
        this.transform.rotation = Quaternion.Euler(0f, 0f, -1* z);
        this.transform.position = nextPos;

        if(endPointReached)
            if(_Entering)
               _Parent.EntanceFinished();
            else
                _Parent.FormationReached();
    }

    public void LoadPath(Bezier path, float speed, bool entrance){
        _EntrancePath = path;
        _Entering = entrance;
        _Speed = speed;
    }

}
