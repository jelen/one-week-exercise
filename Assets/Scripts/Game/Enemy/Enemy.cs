﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Entity
{
    public event Action<Enemy> FinishedEntrance;
    public event Action<Enemy> ReturnedToFormation;
    public event Action<Enemy> AssultStarted;
    public event Action<Enemy> AssultFinished;
    public event Action<Enemy> TakeOff;

    public int FormationSlotIndex;
    [NonSerialized]
    public Bezier BezierPath;

    [SerializeField]
    private EntranceAction _EntranceAction = null;

    private bool _Flying = false;
    private bool _EnteringBattlefield = false;
    private bool _Attacking = false;
    private int _ScoreValue;
    private int _Tier;
    private EnemyDeathAction[] _OnDeathActions;
    private EnemyAttackAction _AttackAction;
    private BattleController _BattleController;
    private EnemyAction _CurrentAction;

    public int ScoreValue{
        get{
            int multiplier = _Flying ? 2 : 1;
            return _ScoreValue * multiplier;
        }
    }

    public int Tier{
        get{
            return _Tier;
        }
    }

    public float MovementSpeed{
        get{
            return _MovementSpeed;
        }
    }

    public bool IsFlying{
        get{
            return _Flying;
        }
    }

    public bool Attacking{
        get{
            return _Attacking;
        }
    }

    public Vector3 FormationPos{
        get;
        set;
    }

    void Update(){
        if(_CurrentAction != null)
            _CurrentAction.UpdateTick(Time.deltaTime/2);

    }

    public void Initialize(BattleController battle, EnemyData data){
        Health = data.Health;
        FormationSlotIndex = -1;

        _BattleController = battle;
        _ScoreValue = data.ScoreValue;
        _MovementSpeed = data.Speed;
        _Tier = data.Tier;
        _EntranceAction.SetUp(this);
        _OnDeathActions = this.GetComponents<EnemyDeathAction>();
        _AttackAction = this.GetComponent<EnemyAttackAction>();

    }

    public void EntanceFinished(){
        _EnteringBattlefield = false;
        _CurrentAction = null;
        FinishedEntrance?.Invoke(this);
    }

    public void FormationReached(){
        _Flying = false;
        _CurrentAction = null;
        this.transform.rotation = Quaternion.Euler(0, 0, 0);
        ReturnedToFormation?.Invoke(this);
    }

    public void FlyIn(Bezier path, bool enter = false){
        if(_Flying == false)
            TakeOff?.Invoke(this);
        
        BezierPath = path;
        BezierPath.ResetTime();
        _EnteringBattlefield = enter;
        _Flying = true;

        _EntranceAction.LoadPath(path, (enter) ? 30 : _MovementSpeed, enter);
        _CurrentAction = _EntranceAction;

    }

    protected override void Collect(){
        foreach(EnemyDeathAction action in _OnDeathActions)
            action.OnDeathAction();

        _BattleController.EnemyKilled(this);
    }

    public void AssultPlayer(Transform target){
        Transform playerShip = target;

        _AttackAction.AttackAction(playerShip, () => {_Attacking = false; _CurrentAction = null; AssultFinished?.Invoke(this);});
        _CurrentAction = _AttackAction as EnemyAction;
        _CurrentAction.SetUp(this);
        _Attacking = true;

    }

}