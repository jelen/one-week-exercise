﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class OnDeathNova : MonoBehaviour, EnemyDeathAction
{

    public void OnDeathAction(){
        Vector2 spawn = this.transform.position;

        Bullet bullet = GameObject.Instantiate(PrefabBinder.Instance.SpikeBullet);
        bullet.transform.SetParent(this.transform.parent, false);
        bullet.SetUp(spawn, Vector2.down, false, 25);

        bullet = GameObject.Instantiate(PrefabBinder.Instance.SpikeBullet);
        bullet.transform.SetParent(this.transform.parent, false);
        bullet.SetUp(spawn, new Vector2(0.707f, -0.707f), false, 25);

        bullet = GameObject.Instantiate(PrefabBinder.Instance.SpikeBullet);
        bullet.transform.SetParent(this.transform.parent, false);
        bullet.SetUp(spawn, new Vector2(-0.707f, -0.707f), false, 25);
    }

}
