﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface EnemyAction
{
    void SetUp(Enemy parent);
    void UpdateTick(float time);
}
