﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyDashAction : MonoBehaviour, EnemyAttackAction, EnemyAction
{

    private bool _IsAttacking = false;
    private Transform _Target;
    private Enemy _Enemy;
    private Bezier _BezierPath;
    private Action _Callback;
    private Vector2 _DashDirection;

    public void UpdateTick(float time){
        if(_IsAttacking == false)
            return;

        bool endPointReached = false;
        Vector2 nextPos, deltaPos;
        if(_DashDirection == Vector2.zero){
            float step = 1/(60/_Enemy.MovementSpeed);
            float interpolationStep = time * step;
            nextPos = _BezierPath.NextStep(interpolationStep, out endPointReached);
        }else{
            float step = _Enemy.MovementSpeed * time;
            nextPos = (Vector2)this.transform.position + _DashDirection * step;
        }

        deltaPos = (Vector2)this.transform.position - (Vector2)_Target.position;
        deltaPos.Normalize();
        float z = Mathf.Atan2(deltaPos.x, deltaPos.y) * Mathf.Rad2Deg;
        this.transform.rotation = Quaternion.Euler(0f, 0f, -1* z);
        this.transform.position = nextPos;

        if(endPointReached){
            StartDash();
        }

        if(IsAttackDone())
            DashEnded();
    }

    public void SetUp(Enemy parent){
        _Enemy = parent;
    }

    public bool IsInRange(Transform target){
        return false;
    }

    private void StartDash(){
        _DashDirection = _Target.position - this.transform.position;
        _DashDirection.Normalize();
    }

    private bool IsAttackDone(){
        Vector3 viewport = Camera.main.WorldToViewportPoint(this.transform.position);
        return viewport.x > 1 || viewport.y > 1 || viewport.x < 0 || viewport.y < 0;
    }

    private void DashEnded(){
        _IsAttacking = false;
        _Callback.Invoke();
    }

    private void SetUpPath(){
        Vector2 controlPoint = new Vector2(UnityEngine.Random.Range(0.1f, 0.9f), UnityEngine.Random.Range(0.1f, 0.9f));
        controlPoint = Camera.main.ViewportToWorldPoint(controlPoint);

        Bezier path = new Bezier(this.transform.position, this.transform.position, _Target.position, controlPoint);
        _BezierPath = path;

    }

    public void AttackAction(Transform target, Action callback){
        _Target = target;
        _IsAttacking = true;
        _Callback = callback;
        _DashDirection = Vector2.zero;

        SetUpPath();
    }


}

