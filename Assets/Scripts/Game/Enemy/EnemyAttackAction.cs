﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface EnemyAttackAction
{

    bool IsInRange(Transform target);
    void AttackAction(Transform target, System.Action callback);
}
