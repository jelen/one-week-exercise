﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class Shooting : MonoBehaviour, EnemyAttackAction, EnemyAction
{   
    public Transform[] ShootingPoints;
    public float AttackCooldown;
    public float ActionRange;
    public int AttackCount;

    private bool _IsAttacking = false;
    private float _AttackDelay;
    private int _AttacksMade;
    private bool _GotIntoRange;
    private Transform _Target;
    private Enemy _Enemy;
    private Bezier _BezierPath;
    private Action _Callback;
    private float _Time = 0;

    public void UpdateTick(float time){
        if(_IsAttacking == false)
            return;

        if(IsInRange(_Target) == false){
            float step = time * _Enemy.MovementSpeed;
            Vector2 nextPos = Vector2.MoveTowards(this.transform.position, _Target.position, step);//_BezierPath.NextStep(interpolationStep, out endPointReached);
            Vector2 deltaPos = (Vector2)this.transform.position - (Vector2)_Target.position;
            deltaPos.Normalize();
            float z = Mathf.Atan2(deltaPos.x, deltaPos.y) * Mathf.Rad2Deg;
            this.transform.rotation = Quaternion.Euler(0f, 0f, -1* z);
            this.transform.position = nextPos;
        }

        _AttackDelay -= time;
        if(_AttackDelay <= 0 && IsInRange(_Target)){
            Shoot();
        }
        
        if(_AttacksMade == AttackCount){
            _IsAttacking = false;
            _Callback.Invoke();
        }
    }

    public void SetUp(Enemy parent){
        _Enemy = parent;
    }

    public bool IsInRange(Transform target){
        bool result = false;
        float dist = Vector3.Distance(this.transform.position, target.position);
        if(ActionRange == -1 || dist <= ActionRange)
            result = true;

        return result;
    }

    public void AttackAction(Transform target, Action callback){
        _Target = target;
        _Time = 0;
        _AttacksMade = 0;
        _AttackDelay = AttackCooldown;
        _IsAttacking = true;
        _Callback = callback;

        SetUpPath();
    }

    private void SetUpPath(){
        Bezier path = new Bezier(this.transform.position, this.transform.position, _Target.position, _Target.position);
        _BezierPath = path;

    }

    private void Shoot(){
        foreach(Transform spawnPoint in ShootingPoints){
            Bullet bullet = GameObject.Instantiate(PrefabBinder.Instance.EnemyBullet);
            bullet.transform.SetParent(this.transform.parent, false);
            bullet.SetUp(spawnPoint.position, (_Target.position - spawnPoint.position).normalized, false, 20);
        }

        _AttacksMade += 1;
        _AttackDelay = AttackCooldown;
    }
    
}
